# GitLab CE in Docker
Multiple-container deployment via Docker Compose

# Installation
```sh
$ git clone https://gitlab.com/KudryashovAV/gitlab-ce-in-docker.git
$ cd gitlab-ce-in-docker
$ docker-compose up --build -d
```
GitLab should be able to access at http://localhost:7000

# Rails
```
gitlab-rails console
Feature.all.map {|f| [f.name, f.state]}
```




# Docs:
https://docs.gitlab.com/omnibus/settings/
https://docs.gitlab.com/omnibus/docker/README.html#install-gitlab-using-docker-compose

https://docs.gitlab.com/ee/administration/repository_storage_paths.html

https://docs.gitlab.com/omnibus/settings/logs.html

https://docs.gitlab.com/ce/install/requirements.html#redis-and-sidekiq

https://docs.gitlab.com/ce/install/requirements.html#postgresql-requirements

https://docs.gitlab.com/ee/administration/environment_variables.html

[change pass](ttps://docs.gitlab.com/ee/security/reset_root_password.html)